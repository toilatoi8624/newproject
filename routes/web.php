<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\ProductCategoryController;
use App\Http\Controllers\User\ProductController;


/**
 * router use to access admincontroller
 */
Route::prefix('/admin')->controller(AdminController::class)->group(function () {
    Route::get  ('/login',      'showLoginForm')            ->name('admin.form_login');
    Route::post ('/login',      'login')                    ->name('admin.login');
    Route::get  ('/logout',     'logout')                   ->name('admin.logout');
    Route::get  ('/register',   'showRegistrationForm')     ->name('admin.register');
    Route::post ('/register',   'register')                 ->name('admin.register');
    Route::get  ('/',           'home')                     ->name('admin.home');
});

/**
 *router group about UserController and  AdminManagerController
 *
 */
Route::prefix('/user')->controller(UserController::class)->group(function () {
    Route::get  ('/login',
        'showLoginForm')
        ->name('user.form_login');
    Route::post ('/login',
        'login')
        ->name('user.login');
    Route::get  ('/logout',
        'logout')
        ->name('user.logout');
    Route::get  ('/register',
        'showRegistrationForm')
        ->name('user.register');
    Route::post ('/register',
        'register')
        ->name('user.register');
    Route::get  ('/',
        'home')
        ->name('user.home');

    Route::get  ('/create',
        'create')
        ->name('admin.create');
    Route::post ('/store',
        'store')
        ->name('admin.store');
    Route::get  ('/edit/{id}',
        'edit')
        ->name('admin.edit');
    Route::put  ('/update/{id}',
        'update')
        ->name('admin.update');
    Route::put  ('/destroy/{id}',
        'destroy')
        ->name('admin.destroy');
    Route::get  ('/index',
        'index')
        ->name('admin.index');
});

Route::prefix('/category')
    ->controller(ProductCategoryController::class)->group(function () {

        Route::get  ('/create',
            'create')
            ->name('category.create');
        Route::post ('/store',
            'store')
            ->name('category.store');
        Route::get  ('/edit/{id}',
            'edit')
            ->name('category.edit');
        Route::put  ('/update/{id}',
            'update')
            ->name('category.update');
        Route::get  ('/destroy/{id}',
            'destroy')
            ->name('category.destroy');
        Route::get  ('/index',
            'index')
            ->name('category.index');
    });


Route::prefix('/product')
->controller(ProductController::class)->group(function () {
Route::get  ('/create',
                            'create')
                                                    ->name('product.create');
Route::post ('/store',
                            'store')
                                                    ->name('product.store');
Route::get  ('/edit/{id}',
                            'edit')
                                                    ->name('product.edit');
Route::put  ('/update/{id}',
                            'update')
                                                    ->name('product.update');
Route::delete  ('/destroy/{id}',
                            'destroy')
                                                    ->name('product.destroy');
Route::get  ('/index',
                            'index')
                                                    ->name('product.index');
Route::get ('/export-csv', 'exportIntoCSV') ->name('product.CSV');
Route::get ('/export-pdf', 'exportIntoPDF') ->name('product.PDF');
});
//
//Route::get('email-test', function(){
//    $info['email'] = 'your_email@gmail.com';
//    dispatch(new App\jobs\SendEmailJob($info));
//    dd('done');
//});

Route::get('/', function () {
    return view('welcome');
});
