<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeEmail extends Mailable
{
    private $details;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     */
    public function __construct()
    {
    }



    /**
     * Build the message.
     *
     * @return WelcomeEmail
     */
    public function build()
    {
        return $this->view('dashboard.admin.user.mail');

    }
}
