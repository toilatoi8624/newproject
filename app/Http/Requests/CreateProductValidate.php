<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateProductValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => [
                'required', 'unique:products', 'string', 'min:10',
                'max:20', 'regex:/^[a-zA-Z0-9]+$/i'
            ],
            'name' => ['required', 'string', 'max:255'],
            'stock' => ['required', 'string', 'max:50'],
            'avatar' => 'required|mimes:png,jpg,jpeg|max:3072',
        ];
    }
}
