<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\SendEmailJob;
use App\Mail\WelcomeEmail;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserValidate;
use App\Http\Requests\AddUserValidate;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    // inheritic AuthenticatesUsers
    use AuthenticatesUsers;

    // biến toàn cục trở tới user
    protected $redirectTo = '/user';

    /**
     *function show Login Form
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login_user');
    }

    /**
     *  function logout
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('user.login');
    }

    /**
     *function show registration Form
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showRegistrationForm()
    {
        return view('auth.register_user');
    }

    /**
     * register
     *
     * @param AddUserValidate $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(AddUserValidate $request)
    {
        event(new Registered($user = $this->create1($request->all())));
        $this->guard()->login($user);

        return redirect()->route('user.login');
    }

    /**
     * input data into database
     *
     * @param array $data
     *
     * @return User
     */
    public static function create1(array $data)
    {
        return User::create(array(
            'user_name' => $data['user_name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'birthday' => $data['birthday'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ));
    }

    /**
     *function home
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home()
    {
        return view('dashboard.user.masterlayout.dashboard');
    }

    /* manager user CRUD about user */
    /**
     *show add form
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('dashboard.admin.user.create');
    }

    /**
     *Add user to database users
     *
     * @param AddUserValidate $request
     *
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddUserValidate $request, User $user)
    {
        $path = $request->file('avatar')->storeAs('/public/upload/user', $request->user_name . '.' . 'jpg');
        $users = User::create([
            'avatar' => $path,
            'user_name' => $request['user_name'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'birthday' => $request['birthday'],
            'email' => $request['email'],
            'password' => Hash::make($request['password'])
        ]);
            $details = [
                "email" => $request->email,
            ];;

            SendEmailJob::dispatch($details);

        return redirect()->route('admin.index', compact('user'));
    }

    /**
     * show form edit
     *
     * @param  $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.admin.user.edit', compact('user'));
    }

    /**
     * Update database users
     *
     * @param UpdateUserValidate $request ,$id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserValidate $request)
    {
        $user = User::findorfail($request->id);
        $user->update(array(
            'user_name' => $request->get('user_name'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'birthday' => $request->get('birthday'),
            'email' => $request->get('email')
        ));
        $details = [
            "email" => $request->email,
        ];;

        SendEmailJob::dispatch($details);


        return redirect()->route('admin.index', compact('user'));
    }

    /**
     *delete record form
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->flag_delete = 1;
        $user->save();
        return redirect()->route('admin.index');
    }

    /**
     * function show data table user
     *
     * @param $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request){
        $searchWord = $request->search_word;
        $users = User::select('id','user_name','first_name','last_name',
            'email', 'birthday','flag_delete');
            if ($request->has('search_word') && !is_null($searchWord)) {
                $users = User::where('user_name','LIKE',"%{$searchWord}%")
                ->orwhere('first_name','LIKE',"%{$searchWord}%")
                ->orwhere('last_name','LIKE',"%{$searchWord}%")
                ->orwhere('email','LIKE',"%{$searchWord}%");
            }

            $users = $users->paginate(10);
        return view('dashboard.admin.user.index',compact('users'));
    }


    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     *
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
