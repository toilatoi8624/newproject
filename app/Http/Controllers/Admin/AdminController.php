<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;


class AdminController extends Controller
{

    use AuthenticatesUsers;
    protected $redirectTo = '/admin';

    /**
     *function show Login For
     *from view auth.login
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     *function home
     *from view auth.login
     */
    public function home()
    {
        return view('dashboard.admin.masterlayout.dashboard');
    }

    /**
     *function show registration Form
     *from view auth.register_user
     */
    public function showRegistrationForm()
    {
        return view('auth.register_user');
    }

    /**
     * validator

     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($admin = $this->reg($request->all())));
        $this->guard()->login($admin);

        if ($response = $this->registered($request, $admin)) {
            return $response;
        }
        return redirect()->route('admin.login');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    // check database form  admin
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     *
     * @return Admin
     */
    protected function reg(array $data)
    {
        return Admin::reg(array(
            'user_name' => $data['user_name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'birthday' => $data['birthday'],
            'password' => Hash::make($data['password']),
        ));
    }

    /**
     *  check session
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('admin.login');
    }
}
