<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Services\deleteCategoryService;

class ProductCategoryController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\ProductCategory  $product_category,
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(ProductCategory $product_category)
    {
        $product_category = ProductCategory::distinct('parent_id')->get();
        return view('dashboard.user.categoryproduct.create', compact('product_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $product_category = ProductCategory::create(array(
            'name'     => $request['name'],
            'parent_id'    => $request['parent_id']
        ));
        $product_category->save();
        return redirect()->route('category.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $product_category_getid = ProductCategory::findorfail($id);
        $product_category_getall = ProductCategory::get();
        return view('dashboard.user.categoryproduct.edit', compact(
            'product_category_getid',
            'product_category_getall'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $product_category_getid = ProductCategory::find($request->id);
        if (isset($product_category_getid) != null) {
            $product_category_getid->name = $request->get('name');
            $product_category_getid->parent_id = $request->get('parent_id');
            $product_category_getid->update();
        } else {
            return redirect()->back();
        }
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $product_category, int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(deleteCategoryService $deleteCategoryService, $id)
    {
        $deleteCategoryService->deleteCategory($id);
        return redirect()->route('category.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $product_category = ProductCategory::select('id', 'name', 'parent_id')->orderby('name')->paginate(15);
        return view('dashboard.user.categoryproduct.index', compact('product_category'));
    }
}
