<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\CreateProductValidate;
use App\Http\Requests\UpdateProductValidate;
use App\Models\product;
use App\Services\deleteProductService;
use App\Services\ProductService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Exports\ProductExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;


class ProductController extends Controller
{


    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {

        $product_category = ProductCategory::get();
        return view('dashboard.user.product.create', compact('product_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductService $productService
     * @param CreateProductValidate $request
     *
     * @return RedirectResponse
     */
    public function store(ProductService $productService, CreateProductValidate $request)
    {
        $path = $productService->uploadImage($request);
        if($request->hasFile('avatar')){
            $path =substr($request->file('avatar')
                ->storeAs('upload/product', $request->sku . '.' . 'jpg'), strlen('public/'));
            $product = Product::create(array(
                'name' => $request->name,
                'stock' =>$request->stock,
                'expired_at' => $request->expired_at,
                'avatar' => $path,
                'sku' => $request->sku,
                'category_id' => $request->category_id,
            ));
        }
        return redirect()->route('product.index')->with('statuscreate', 'Bạn đã thêm thành công vào dữ liệu');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param product $product ,int $id
     *
     * @return Application|Factory|View
     */
    public function edit(Product $product, $id)
    {
        $product = Product::findorfail($id);
        $product_category_getall = ProductCategory::get();
        $product_category = ProductCategory::select('id', 'name')
            ->pluck('name', 'id')->toArray();
        return view(
            'dashboard.user.product.edit',
            compact('product', 'product_category', 'product_category_getall')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductValidate $request
     * @param ProductService $productService
     * @param product $product
     *
     * @return RedirectResponse
     */
    public function update(UpdateProductValidate $request, ProductService $productService, Product $product)
    {
        $path = $productService->uploadImage($request);
        $product = Product::findorfail($request->id);
        $product->update(array(
            'name' => $request->name,
            'stock' => $request->stock,
            'expired_at' => $request->expired_at,
            'avatar' => $path,
            'sku' => $request->sku,
            'category_id' => $request->category_id,
        ));

        $product->save();
        return redirect()->route('product.index')->with('statusupdate', 'Bạn đã cập nhật dữ liệu thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param deleteProductService $deleteProductService
     * @param $id
     *
     * @return RedirectResponse
     */
    public function destroy(deleteProductService $deleteProductService, $id)
    {
        $deleteProductService->deleteProduct($id);
        return redirect()->route('product.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     *
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $searchWord = $request->search_word;
        $stock = $request->filter_stock;
        $product = Product::select('id', 'name', 'stock', 'avatar', 'sku', 'expired_at');
        if ($request->has('search_word') && !is_null($searchWord)) {
            $product->where('name', 'LIKE', "%{$searchWord}%");
        }

        if ($request->has('filter_stock')) {
            switch ($stock) {
                case 'lower10':
                    $product->where('stock', '<', 10);
                    break;
                case 'between10to100':
                    $product->whereBetween('stock', [10, 100]);
                    break;
                case 'between100to200':
                    $product->whereBetween('stock', [100, 200]);
                    break;
                case 'higher200':
                    $product->where('stock', '>', 200);
                    break;
            };
        }

        $product = $product->paginate(15);
        return view('dashboard.user.product.index', compact('product'));
    }

    /**
     * Download file csv
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */

    public function exportIntoCSV()
    {
        return Excel::download(new ProductExport, 'ProductList.csv');
    }

    /**
     * Download file PDF
     *
     * @return \Illuminate\Http\Response
     */
    public function exportIntoPDF()
    {
        $data['product'] = product::select('id', 'sku', 'name')->get();
        $pdf = PDF::loadView('dashboard.user.product.pdf', $data);
        return $pdf->download('product.pdf');
    }
}
