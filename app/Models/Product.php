<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * class admin
    * @property interger    $id
    * @property string      $sku
    * @property string      $name
    * @property interger    $stock
    * @property string      $avatar
    * @property date        $expired_at
    * @property interger    $category_id
    * @property date        $created_at
    * @property date        $updated_at
    * @property interger    $flag_delete

*/
class Product extends Model
{
    use HasFactory;

    protected $guard = 'product';

    protected $fillable = [
        'id',
        'sku',
        'name',
        'stock',
        'avatar',
        'expired_at',
        'category_id',
        'flag_delete',
    ];
    /**
     * get data Product
     *
     * @return array
     */
    public static function getProduct(){
        $records = DB::table('products')->select('id','sku','name','stock','avatar','expired_at')->get()->toArray();
        return $records;
    }

    /**
     * @inherit ProductCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productcategory() {
        return $this->belongsTo(ProductCategory::class);
    }
}
