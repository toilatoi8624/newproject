<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;


/**
 * class admin
 * @property interger    $id
 * @property string      $email
 * @property string      $user_name
 * @property date      $birthday
 * @property string      $first_name
 * @property string      $last_name
 * @property string      $password
 * @property string      $reset_password
 * @property string      $status
 * @property interger    $flag_delete
 * @property date        $created_at
 * @property date        $updated_at
 */
class User extends Authenticatable implements AuthenticatableInterface
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable = [
        'id',
        'email',
        'avatar',
        'user_name',
        'birthday',
        'first_name',
        'last_name',
        'password',
        'reset_password',
        'status',
        'flag_delete',
        'created_at',
        'updated_at',
    ];


}
