<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * class product category
    * @property interger        $id
    * @property string      $name
    * @property interger        $parent_id
    * @property date        $created_at
    * @property date        $updated_at

 */
class ProductCategory extends Model
{
    use HasFactory;
    protected $guard = 'productcategory';

    protected $fillable = [
        'id',
        'name',
        'parent_id',
        'created_at',
        'updated_at'
    ];

    /**
     * @inherit producto
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product() {
        return $this->hasMany(Product::class);
    }
}
