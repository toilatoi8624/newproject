<?php

namespace App\Services;

use App\Models\Product;
use http\Env\Request;

/**
 *
 */
class ProductService {

    /**
     * uploadImage
     * @param Request $request
     * @return false|string
     */
    public function uploadImage($request)
    {
        if ($request->hasFile('avatar')) {
            $path = substr($request->file('avatar')
                ->storeAs('public/upload/product', $request->sku . '.' . 'jpg'), strlen('public/'));
        }
        return $path;
    }

}


