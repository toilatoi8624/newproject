<?php

namespace App\Services;

use App\Models\ProductCategory;

class deleteCategoryService {

    /**
     * @param $Product_id
     * @return string
     */
    public function deleteCategory($Category_id)
    {
        $product_category = ProductCategory::find($Category_id);
        $product_category->delete();
    }
}

