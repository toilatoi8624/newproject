<?php

namespace App\Services;

use App\Models\Product;

class deleteProductService {

    /**
     * @param $Product_id
     * @return string
     */
    public function deleteProduct($Product_id)
    {
        $product = Product::find($Product_id);
        $product->delete();
    }
}

