<?php

namespace App\Console\Commands;

use App\Jobs\SendHappyBirthDay;
use Illuminate\Console\Command;

class sendHBD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:sendHBD';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $details = [
            "email" => 'toilatoi8624@gmail.com',
        ];;
        SendHappyBirthDay::dispatch($details);
        return 0;
    }
}
