<?php

namespace App\Exports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductExport implements FromCollection,WithHeadings
{

    /**
     * export file
     *
     * @return void
     */
        public function headings():array{
        return [
            'id',
            'sku',
            'name',
            'stock',
            'avatar',
            'expired_at',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(Product::getProduct());
    }
}
