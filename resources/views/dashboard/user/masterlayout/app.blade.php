<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        import swal from 'sweetalert';
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
        <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/fontawesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
        </div>

        @include('dashboard.user.masterlayout.header')
        @include('dashboard.user.masterlayout.siderbar')
        @yield('content')
        @include('dashboard.user.masterlayout.footer')


        <script src="{{asset('js/image.js')}}"></script>
        <script src="{{asset('js/preview.js')}}"></script>
        <script src="{{asset('js/delete.js')}}"></script>
        <script src="{{asset('js/sweetalert.js')}}"></script>
        <script src="{{ asset('js/search.js') }}"></script>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>


        <script>
            @if (session('statuscreate'))
                swal({
                    title: '{{ session('status') }}',
                    text: "bạn đã thêm thành công!",
                    icon: "success",
                    button: "Ok",
                  });
            @endif
        </script>
        <script>
            @if (session('statusupdate'))
                swal({
                    title: '{{ session('status') }}',
                    text: "bạn đã thay đổi thành công!",
                    icon: "success",
                    button: "Ok",
                  });
            @endif
        </script>
    </body>

</html>
