<div id="search" class="row search" style="margin: 20px -0px 20px 270px ">
    <h2 class="text-center" style="font-style: bold">Search</h2>
    <form action="">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <select name="filter_stock" class="select2 select2-hidden-accessible"style="width: 100%;" data-select2-id="1"
                                tabindex="-1" aria-hidden="true">
                                <option></option>
                                <option value="lower10">Stock < 10</option>
                                <option value="between10to100">Stock > 10 && Stock <100 </option>
                                <option value="between100to200">Stock > 100 && Stock <200 </option>
                                <option value="higher200">Stock > 200 </option>
                            </select>
                        </div>
                    </div>
                </div>
                <form action="{{ route('product.index') }}" method="GET" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control " placeholder="Type your keywords here"  name="search_word">
                        <span class="input-group-btn mr-5 mt-1">
                            <button class="btn btn-info" type="submit" title="Search projects">
                                <span class="fas fa-search"></span>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </form>
</div>
