@extends('dashboard.user.masterlayout.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ __('Create Category') }}
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{route('product.store')}}" enctype="multipart/form-data" name="formName">
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Sku') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="input_sku form-control @error('sku') is-invalid @enderror"
                                           name="sku" value="{{ old('sku') }}" required autocomplete="name" autofocus>

                                    @error('sku')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name Product') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="input_name form-control @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') }}" required autocomplete="name " autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Stock') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="input_stock form-control @error('stock') is-invalid @enderror"
                                           name="stock"class="input_stock" value="{{ old('stock') }}" required autocomplete="stock " autofocus>
                                    @error('stock')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Avatar') }}</label>

                                <div class="col-md-6">
                                    <input type='file' name="avatar" onchange="readURL(this);" />
                                    <img id="ImdID" width="180" src="" alt="" />

                                    @error('avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Expired At	') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="date" class="input_expired form-control input_expired @error('expired_at') is-invalid @enderror"
                                           name="expired_at"value="{{ old('expired_at	') }}" required autocomplete="expired_at" autofocus>
                                    @error('stock')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <select style="text-align: center;margin-left: 45%;padding: 10px" class="select_category" name="category_id">
                                    <option>-- Select --</option>
                                    @foreach ($product_category as $data)
                                        <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button style="float: left"   style="margin: 10px 10px 10px 100px" type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                    <button style="float: left;margin-left: 100px" onclick="OpenProduct()"   style="margin: 10px 10px 10px 100px" type="button" class="btn btn-primary">
                                        {{ __('preview') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="lightbox-blanket" style="color:white;
                                background-color: #1CCAD8;
                                padding: 12px;
                                height:35%;
                                width:46%;
                                position: absolute;
                                left:27%;
                                top:13%;
                                z-index: 2;display:none">
            <div class="pop-up-container">
                <div class="pop-up-container-vertical">
                    <div class="pop-up-wrapper">
                        <div class="go-back" onclick="GoBack();">
                            <i class="fa fa-arrow-left"></i>
                        </div>
                        <div class="product-details">
                            <div class="product-left" style="float: left">
                                <div style="font-size: 20px" class="product-info">
                                    <label for="">SKU:</label>
                                    <label class="show_sku"></label><br>
                                    <label for="">Name:</label>
                                    <label class="show_name"></label><br>
                                    <label for="">Stock:</label>
                                    <label class="show_stock"></label><br>
                                    <label for="">Expired:</label>
                                    <label class="show_expired"></label><br>
                                    <label for="">Category:</label>
                                    <label class="show_category"></label><br>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-image">
                                    <img class="show_img" style="float:left;margin:  0 15% 5% 15%" id="previewAvatar" src="{{asset('storage/'.$data->avatar)}}" height="200" width="200">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

