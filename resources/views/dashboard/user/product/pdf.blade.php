<table class="table table-hover text-nowrap">
    <thead>
        <tr>
            <th>ID</th>
            <th>Sku</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($product as $key => $data)
            <tr>
                <td>{{ $data->id }}</td>
                <td>{{ $data->sku }}</td>
                <td>{{ $data->name }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
