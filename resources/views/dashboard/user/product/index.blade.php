@extends('dashboard.user.masterlayout.app')
@section('content')
@include('dashboard.user.product.search')
    <div class="row" style="display: block;margin: 20px -0px 20px 270px ">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a style="padding: 10px;margin-right: 10px;color: white;background-color: #3C8DBC" href="">Tác
                        Vụ</a>
                    <a style="padding: 10px;margin-right: 10px;color: white;background-color: #3C8DBC" href="">làm mới
                    </a>
                    <a href="{{ route('product.CSV') }}"
                        style="padding: 10px;margin-right: 10px;color: white;background-color: #75b2d6" href="">Tải
                        file
                        file csv</a>
                    <a href="{{ route('product.PDF') }}"
                        style="padding: 10px;margin-right: 10px;color: white;background-color: #71a763" href="">Tải
                        file
                        PDF</a>
                        <button style="border:none; padding: 8px;margin-right: 10px;color: white;background-color: #3C8DBC"
                        onclick="myFunction()">Tìm kiếm</button>
                    <div style=" background-color: limegreen;float: right;margin-right: 60px;padding: 10px 20px">
                        <i style="color:white" class="fa-solid fa-floppy-disk"></i>
                        <a style="color:white" href="{{ route('product.create') }}">Thêm mới</a>
                    </div>
                </div>

                <div class="card-body table-responsive p-0">
                    @if (Session::get('success', false))
                        <?php $data = Session::get('success'); ?>
                        @if (is_array($data))
                            @foreach ($data as $msg)
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-check"></i>
                                    {{ $msg }}
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-success" role="alert">
                                <i class="fa fa-check"></i>
                                {{ $data }}
                            </div>
                        @endif
                    @endif

                    <table class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Sku</th>
                                <th>Name</th>
                                <th>stock</th>
                                <th>Avatar</th>
                                <th>Expired At</th>
                                <th style="" colspan="2">Tác vụ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product as $key => $data)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $data->sku }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->stock }}</td>
                                    <td>
                                        @if ($data->avatar)
                                            <img src="{{ asset('storage/' . $data->avatar) }}" height="120" width="120"
                                                alt="{{ $data->name }}">
                                        @endif
                                    </td>
                                    </td>
                                    <td>{{ $data->expired_at }}</td>
                                    <td>
                                        <a style="float: left;margin-right: 10px"
                                            href="{{ route('product.edit', $data->id) }}">
                                            <button
                                                style="border: none;color: white;background-color: limegreen"type="submit">
                                                Edit
                                            </button>
                                        </a>
                                        <form method="post"  class="delete-form-{{ $data->id }}"
                                              action="{{ route('product.destroy', $data->id) }}"
                                        >
                                            @csrf
                                            @method('delete')
                                            <button type="button" onclick="deleteProduct('delete-form-{{ $data->id }}')" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $product->links() }}
        </div>
    </div>

@endsection
