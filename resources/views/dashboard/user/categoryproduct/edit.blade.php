@extends('dashboard.user.masterlayout.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ __('Create Category') }}
                    </div>

                    <!-- Container (Contact Section) -->
                    <div class="card-body">
                        <form method="POST" action="{{ route('category.update', $product_category_getid->id) }}">
                            @csrf
                            @method('put')
                            <div class="row mb-3">
                                <label for="name"
                                    class="col-md-4 col-form-label text-md-end">{{ __('Category Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ $product_category_getid->name }}" required autocomplete="name " autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <select style="text-align: center;margin-left: 45%;padding: 10px" name="parent_id">
                                    <option value="">--select--</option>

                                    @foreach ($product_category_getall as $data)
                                        @if ($product_category_getid['parent_id'] == null)
                                            @if ($data->parent_id == null)
                                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                            @endif
                                        @endif
                                    @endforeach

                                </select>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button style="margin: 10px 10px 10px 100px" type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
