@extends('dashboard.user.masterlayout.app')
@section('content')
    <div class="row" style="display: block;margin: 20px -0px 20px 270px ">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a style="padding: 10px;margin-right: 10px;color: white;background-color: #3C8DBC" href="">Tác
                        Vụ</a>
                    <a style="padding: 10px;margin-right: 10px;color: white;background-color: #3C8DBC" href="">làm mới
                    </a>
                    <a style="padding: 10px;margin-right: 10px;color: white;background-color: #3C8DBC" href="">Tìm
                        kiếm</a>
                    <div style=" background-color: limegreen;float: right;margin-right: 60px;padding: 10px 20px">
                        <i style="color:white" class="fa-solid fa-floppy-disk"></i>
                        <a style="color:white" href="{{ route('category.create') }}
                               ">Thêm
                            mới</a>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Category Name</th>
                                <th>Parent ID</th>
                                <th style="" colspan="2">Tác vụ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product_category as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->parent_id }}</td>
                                    <td>
                                        <a style="float: left;margin-right: 10px"
                                            href="{{ route('category.edit', $data->id) }}">
                                            <button style="border: none;color: white;background-color: limegreen"
                                                type="submit">
                                                Edit
                                            </button>
                                        </a>
                                        <form action="{{ route('category.destroy', $data->id) }}" method="get">
                                            @csrf
                                            <button
                                                style="border: none;color:
                                             white;background-color: orangered"
                                                type="submit">
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            {{ $product_category->links() }}
            <!-- /.card -->
        </div>
    </div>
@endsection
