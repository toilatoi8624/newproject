@extends('dashboard.user.masterlayout.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Create Category') }}
                </div>

                <!-- Container (Contact Section) -->
                <div class="card-body">
                    <form method="POST" action="{{route('category.store')}}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('User Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name " autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div>
                            <select style="text-align: center;margin-left: 45%;padding: 10px" name="parent_id">
                                <option> </option>
                                @foreach ($product_category as $data)
                                <option value="{{$data->parent_id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button style="margin: 10px 10px 10px 100px" type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection