@extends('dashboard.admin.masterlayout.app')
@section('content')
    <div class="row" style="display: block;margin: 20px -0px 20px 270px ">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a style="padding: 10px;margin-right: 10px;color: white;background-color: #3C8DBC" href="">Tác
                        Vụ</a>
                    <a style="padding: 10px;margin-right: 10px;color: white;background-color: #3C8DBC" href="">làm mới
                    </a>
                    <a style="padding: 10px;margin-right: 10px;color: white;background-color: #3C8DBC" href="">Tìm
                        kiếm</a>
                    <div style=" background-color: limegreen;float: right;margin-right: 60px;padding: 10px 20px">
                        <i style="color:white" class="fa-solid fa-floppy-disk"></i>
                        <a style="color:white" href="{{ route('admin.adduser') }}">Thêm mới</a>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>Họ</th>
                                <th>Tên</th>
                                <th>Email</th>
                                <th>Ngày sinh</th>
                                <th style="display: inline-block; margin-left: 20px" colspan="2">Tác vụ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $data)
                                @if ($data->flag_delete === 1)
                                @endif
                                @if ($data->flag_delete === 0)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->user_name }}</td>
                                        <td>{{ $data->last_name }}</td>
                                        <td>{{ $data->first_name }}</td>
                                        <td>{{ $data->email }}</td>
                                        <td>{{ $data->birthday }}</td>
                                        <td>
                                            <a style="float: left;margin-right: 10px"
                                                href="{{ route('admin.edituser', $data->id) }}">
                                                <button style="border: none;color: white;background-color: limegreen"
                                                    type="submit">
                                                    Edit
                                                </button>
                                            </a>
                                            <form action="{{ route('admin.deleteuser', $data->id) }}" method="post">
                                                @method('put')
                                                @csrf
                                                <button
                                                    style="border: none;color:
                                             white;background-color: orangered"
                                                    type="submit">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            {{ $users->links() }}
            <!-- /.card -->
        </div>
    </div>
@endsection
