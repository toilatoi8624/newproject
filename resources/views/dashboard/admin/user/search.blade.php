<div id="search" class="row search" style="display:none;margin: 20px -0px 20px 270px ">
    <h2 class="text-center" style="font-style: bold">Search</h2>
    <form action="">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <form action="{{ route('admin.index') }}" method="GET" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control " placeholder="Type your keywords here"  name="search_word">
                        <span class="input-group-btn mr-5 mt-1">
                            <button class="btn btn-info" type="submit" title="Search projects">
                                <span class="fas fa-search"></span>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </form>
</div>
