function deleteProduct(classSubmit) {
    Swal.fire({
        title: 'Are you sure you want to delete this record?',
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
    }).then((result) => {
        $(`.${classSubmit}`).submit()
    });
}
