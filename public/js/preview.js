function OpenProduct(i) {
    $(".lightbox-blanket").toggle();
    var inputName = document.querySelector('.input_name');
        var inputStock = document.querySelector('.input_stock');
        var inputExpired = document.querySelector('.input_expired');
        var inputSku = document.querySelector('.input_sku');
        var inputCategory = document.querySelector('.select_category');
        var file    = document.querySelector('input[type=file]').files[0];
        var showImg = document.querySelector('.show_img');
        var reader  = new FileReader();
        document.querySelector('.show_name').innerText = inputName.value ;
        document.querySelector('.show_stock').innerText = inputStock.value;
        document.querySelector('.show_expired').innerText = inputExpired.value;
        document.querySelector('.show_sku').innerText = inputSku.value;
        document.querySelector('.show_category').innerText = inputCategory.options[inputCategory.selectedIndex].text;
        reader.onloadend = function () {
            showImg.setAttribute("src", this.result);
        }
        if (file) {
         reader.readAsDataURL(file);
            }
}
function GoBack() {
    $(".lightbox-blanket").toggle();
}
